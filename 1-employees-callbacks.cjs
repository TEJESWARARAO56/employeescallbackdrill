const fs = require('fs');


const data = require('./data.json')

function performoperation(ids) {

    const promiseObj = new Promise((resolve, reject) => {
        try {
            const dataOfIds = data.employees.filter((employee) => ids.includes(employee.id))
            // console.log(dataOfIds)
            resolve(dataOfIds)
        } catch (error) {
            reject(error)
        }
    })
        .then((dataOfIds) => {
            fs.writeFileSync("./dataOfParticularIds.json", JSON.stringify(dataOfIds), (err) => {
                if (err) {
                    throw new Error(error)
                }
            })
        })
        .then(() => {
            const groupedDataByCompanies = data.employees.reduce((accumulator, employee) => {
                accumulator[employee['company']] = accumulator[employee['company']] || []
                accumulator[employee['company']].push(employee)
                return accumulator
            }, {})
            fs.writeFileSync("./groupedDataByCompany.json", JSON.stringify(groupedDataByCompanies), (err) => {
                if (err) {
                    throw new Error(error)
                }
            })
        })
        .then(() => {
            const dataOfPowerpuffBrigade = data.employees.filter((employee) => employee.company === 'Powerpuff Brigade')
            fs.writeFileSync("./powerpuffBrigade.json", JSON.stringify(dataOfPowerpuffBrigade), (err) => {
                if (err) {
                    throw new Error(error)
                }
            })
        })
        .then(() => {
            const dataWithoutId2 = data.employees.filter((employee) => employee.id !== 2)
            fs.writeFileSync("./dataWithoutId2.json", JSON.stringify(dataWithoutId2), (err) => {
                if (err) {
                    throw new Error(error)
                }
            })
        })
        .then(() => {
            const sortedEmployeesDate = data.employees.sort((employee1, employee2) => {
                if (employee1.company === employee2.company) {
                    return employee1.id - employee2.id
                }
                return employee1.company > employee2.company ? 1 : -1
            })
            fs.writeFileSync("./sortedEmployeesDate.json", JSON.stringify(sortedEmployeesDate), (err) => {
                if (err) {
                    throw new Error(error)
                }
            })
        })
        .then(() => {
            const indexOfId93 = data.employees.findIndex((employee) => {
                return employee.id === 93;
            })
            const indexOfId92 = data.employees.findIndex((employee) => {
                return employee.id === 92;
            })
            let swapedData = data
            let dataOfId93 = swapedData.employees[indexOfId92];
            swapedData.employees[indexOfId92] = swapedData.employees[indexOfId93];
            swapedData.employees[indexOfId93] = dataOfId93;
            fs.writeFileSync("./swapedData.json", JSON.stringify(swapedData), (err) => {
                if (err) {
                    throw new Error(error)
                }
            })
        })
        .then(() => {
            const dataAfterAddingBithday = data.employees.map((employee) => {
                if (employee.id % 2 === 0) {
                    employee.birthday = new Date().toDateString()
                }
                return employee
            })
            fs.writeFileSync("./dataAfterAddingBithday.json", JSON.stringify(dataAfterAddingBithday), (err) => {
                if (err) {
                    throw new Error(error)
                }
            })

        })
        .catch((error) => {
            console.error(error)
        })
}

performoperation([2, 13, 23])
