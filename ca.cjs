const fs = require('fs');
const data = require('./data.json')

function performusingcallback() {
    try {
        const dataOfIds = data.employees.filter((employee) => [2, 13, 23].includes(employee.id))
        fs.writeFile("./output/dataOfParticularIds.json", JSON.stringify(dataOfIds), (err) => {
            if (err) {
                throw new Error(error)
            }
            const groupedDataByCompanies = data.employees.reduce((accumulator, employee) => {
                accumulator[employee['company']] = accumulator[employee['company']] || []
                accumulator[employee['company']].push(employee)
                return accumulator
            }, {})
            fs.writeFile("./output/groupedDataByCompany.json", JSON.stringify(groupedDataByCompanies), (err) => {
                if (err) {
                    throw new Error(error)
                }
                const dataOfPowerpuffBrigade = data.employees.filter((employee) => employee.company === 'Powerpuff Brigade')
                fs.writeFile("./output/powerpuffBrigade.json", JSON.stringify(dataOfPowerpuffBrigade), (err) => {
                    if (err) {
                        throw new Error(error)
                    }
                    const dataWithoutId2 = data.employees.filter((employee) => employee.id !== 2)
                    fs.writeFile("./output/dataWithoutId2.json", JSON.stringify(dataWithoutId2), (err) => {
                        if (err) {
                            throw new Error(error)
                        }
                        const sortedEmployeesDate = data.employees.sort((employee1, employee2) => {
                            if (employee1.company === employee2.company) {
                                return employee1.id - employee2.id
                            }
                            return employee1.company > employee2.company ? 1 : -1
                        })
                        fs.writeFile("./output/sortedEmployeesDate.json", JSON.stringify(sortedEmployeesDate), (err) => {
                            if (err) {
                                throw new Error(error)
                            }
                            const indexOfId93 = data.employees.findIndex((employee) => {
                                return employee.id === 93;
                            })
                            const indexOfId92 = data.employees.findIndex((employee) => {
                                return employee.id === 92;
                            })
                            let swapedData = data
                            let dataOfId93 = swapedData.employees[indexOfId92];
                            swapedData.employees[indexOfId92] = swapedData.employees[indexOfId93];
                            swapedData.employees[indexOfId93] = dataOfId93;
                            fs.writeFile("./output/swapedData.json", JSON.stringify(swapedData), (err) => {
                                if (err) {
                                    throw new Error(error)
                                }
                                const dataAfterAddingBithday = data.employees.map((employee) => {
                                    if (employee.id % 2 === 0) {
                                        employee.birthday = new Date().toDateString()
                                    }
                                    return employee
                                })
                                fs.writeFile("./output/dataAfterAddingBithday.json", JSON.stringify(dataAfterAddingBithday), (err) => {
                                    if (err) {
                                        throw new Error(error)
                                    }
                                })
                            })
                        })
                    })
                })
            })
        })
    } catch (error) {
        console.error(error)
    }
}

performusingcallback()